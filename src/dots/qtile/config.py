from typing import List

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy

from myqtile.keybinds import keys
from myqtile.globals import (
    modKey,
    FONT_PRIMARY,
    colorpal,
    myTerminal,
    myEditor,
    myBrowser,
    myPlayer)
from myqtile.layouts import layouts

import subprocess

subprocess.call("xrandr --output HDMI-0 --rate 144 --mode 2560x1440 --left-of DVI-D-0 --primary", shell=True)
subprocess.call("xrandr --output DVI-D-0 --rate 60 --mode 1920x1080 --right-of HDMI-0", shell=True)
subprocess.call("lxqt-session &", shell=True)
subprocess.call("nitrogen --restore &", shell=True)
subprocess.call("qjackctl &", shell=True)
#subprocess.call("notify-send $(echo ${PWD})", shell=True)

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([modKey], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([modKey, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
    ])

widget_defaults = dict(
    font=FONT_PRIMARY[0],
    fontsize=FONT_PRIMARY[1],
    padding=4,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        'launch': (colorpal["backgroundalt"], colorpal["foregroundalt"]),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                #widget.TextBox(subprocess.check_output(["/bin/bash","-c","uname -r"]).decode('utf-8'), name="default"),
                widget.Systray(),
                #widget.Clock(format='%Y-%m-%d %a %I:%M %p'),
                widget.Clock(format="[%m/%d/%Y %I:%M %p]")
            ],
            24,
            background=colorpal["background"],
            opacity=0.88
        ),
    ),
]

keys = keys

# Drag floating layouts.
mouse = [
    Drag([modKey], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([modKey], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([modKey], "Button2", lazy.window.bring_to_front())
]

floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = True
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True
auto_minimize = True
wmname = "LG3D"
