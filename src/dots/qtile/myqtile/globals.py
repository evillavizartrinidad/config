import json
import os

modKey = "mod4"

# Application preferences
myTerminal = "alacritty"
myEditor = "nvim"
myBrowser = "librewolf"
myPlayer = "mpv"
myFilemanager = "pcmanfm"

notes_dir = os.path.expanduser("~/Documents/qtile-notes")
notes_default_filename = "notes.md"
if not os.path.exists(notes_dir):
    os.mkdir(notes_dir)
if not os.path.exists(os.path.join(notes_dir, notes_default_filename)):
    with open(os.path.join(notes_dir, notes_default_filename),"w") as f:
        f.write("Write some notes..")
        f.close()

FONT_PRIMARY = ["hack nerd font mono", 12]

colorpal = json.load(open(os.path.expanduser("~/.config/qtile/myqtile/colors.json")))

layout_theme = {
    "border_width": 2,
    "margin": 8,
    "border_focus": colorpal["foregroundalt"],
    "border_normal": colorpal["accent1"]
}
