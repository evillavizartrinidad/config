from libqtile import layout
from .globals import layout_theme, colorpal

layouts = [
    #layout.MonadWide(**layout_theme),
    #layout.Bsp(**layout_theme),
    layout.Stack(stacks=2, 
        border_focus="#"+colorpal["foregroundalt"],
        border_normal="#"+colorpal["backgroundalt"],
        border_width=1,
        margin=7),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.Tile(shift_windows=True, **layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    #layout.Zoomy(**layout_theme),
    layout.MonadTall(
        border_focus="#"+colorpal["foregroundalt"],
        border_normal="#"+colorpal["backgroundalt"],
        border_width=1,
        margin=4
    ),
    layout.Max(
        border_focus="#"+colorpal["foregroundalt"],
        border_normal="#"+colorpal["backgroundalt"],
        border_width=0,
        margin=0
    ),
    layout.Stack(num_stacks=2,
        border_width=1,
        margin=7),
    layout.RatioTile(
        border_focus="#"+colorpal["foregroundalt"],
        border_normal="#"+colorpal["backgroundalt"],
        border_width=1,
        margin=7
    ),
    layout.TreeTab(
         font = "Ubuntu",
         fontsize = 12,
         sections = ["FIRST", "SECOND", "THIRD", "FOURTH"],
         section_fontsize = 10,
         border_width = 1,
         bg_color = colorpal["background"],
         active_bg = colorpal["accent2"],
         active_fg = colorpal["foreground"],
         inactive_bg = colorpal["foregroundalt"],
         inactive_fg = colorpal["accent3"],
         padding_left = 4,
         padding_x = 4,
         padding_y = 4,
         section_top = 8,
         section_bottom = 20,
         level_shift = 8,
         vspace = 3,
         panel_width = 200,
         margin=0
         ),
    layout.Floating(
        border_focus="#"+colorpal["foregroundalt"],
        border_normal="#"+colorpal["backgroundalt"],
        border_width=1,
        margin=0
    ),
    layout.Columns(border_focus_stack="#"+colorpal["accent1"],
        border_focus="#"+colorpal["foregroundalt"],
        border_normal="#"+colorpal["backgroundalt"],
        border_width=1,
        margin=4)
]

