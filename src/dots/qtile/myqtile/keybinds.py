from libqtile.lazy import lazy
from libqtile.config import Key
import os
from .globals import (
    modKey, myTerminal, 
    myBrowser, myPlayer,
    myEditor,
    myFilemanager,
    notes_dir, 
    notes_default_filename)

keys = [
    # Switch between windows
    Key([modKey], "h",
        lazy.layout.left(),
        desc="Move focus left"),
    Key([modKey], "l",
        lazy.layout.right(),
        desc="Move focus right"),
    Key([modKey], "j",
        lazy.layout.down(),
        desc="Move focus down"),
    Key([modKey], "k",
        lazy.layout.up(),
        desc="Move focus up"),
    Key([modKey], "space",
        lazy.layout.next(),
        desc="Focus next window"),
    Key([modKey], "i",
        lazy.window.toggle_floating(),
        desc="Toggle floating"),
    Key([modKey], "o",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen"),
    Key([modKey], "p",
        lazy.window.toggle_minimize(),
        desc="Toggle minimize"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([modKey, "shift"], "h",
        lazy.layout.shuffle_left(),
        desc="Move window left"),
    Key([modKey, "shift"], "l",
        lazy.layout.shuffle_right(),
        desc="Move window right"),
    Key([modKey, "shift"], "j",
        lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([modKey, "shift"], "k",
        lazy.layout.shuffle_up(),
        desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([modKey, "control"], "h", lazy.layout.grow_left(),
        desc="Extend window left"),
    Key([modKey, "control"], "l", lazy.layout.grow_right(),
        desc="Extend window right"),
    Key([modKey, "control"], "j", lazy.layout.grow_down(),
        desc="Extend window down"),
    Key([modKey, "control"], "k",
        lazy.layout.grow_up(),
        desc="Extend window up"),
    Key([modKey], "n",
        lazy.layout.normalize(),
        desc="Normalize all windows"),

    # Toggle between split and unsplit sides of stack.
    Key([modKey, "shift"], "Return",
        lazy.layout.toggle_split(),
        desc="Toggle window stack split"),
    # Launch software
    Key([modKey], "Return",
        lazy.spawn(myTerminal),
        desc="Launch terminal"),
    Key([modKey, "shift"], "Return",
        lazy.spawn(myFilemanager)),
    Key([modKey], "m",
        lazy.spawn("mousemode"),
        desc="Launch mousemode, quit with Q"),
    Key([modKey], "b",
        lazy.spawn(myBrowser),
        desc="Launch browser"),
    #Key([modKey], "m",
    #    lazy.spawn(myPlayer),
    #    desc="Launch media player"),
    # Toggle between different layouts as defined below
    Key([modKey], "Tab",
        lazy.next_layout(),
        desc="Cycle layouts"),
    Key([modKey], "q",
        lazy.window.kill(),
        desc="Kill focused window"),

    Key([modKey, "control"], "r",
        lazy.restart(),
        desc="Restart Qtile"),
    Key([modKey, "control"], "q",
        lazy.shutdown(),
        desc="Exit Qtile"),
    Key([modKey], "r",
        lazy.spawncmd(),
        desc="Launch run command prompt"),
    Key([modKey, "shift"], "r",
        lazy.spawn("rofi -theme ~/.config/rofi/themes/ayu.rasi -show drun -show-icons -icon-theme \"Adwaita\""))
]
