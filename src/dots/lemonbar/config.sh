#!/usr/bin/bash

SEP_ICON_RIGHT="\uf053"
SEP_ICON_LEFT="\uf054"

GetVolume() {
    VOLUMEPERC=$(pulseaudio-control output | cut -d ' ' -f1)
    echo "$VOLUMEPERC"
}

GetTime() {
    DATE_TIME=$(date "+%T")
    echo -n "$DATE_TIME"
}

GetDate() {
    DATE_DATE=$(date "+%D")
    echo -n "$DATE_DATE"
}

GetMemory() {
    #MEMUSEDGB=$(free | grep Mem | awk '{ printf("%.4f %\n", $4/$2 * 100.0) }' | cut -b -4)
    MEMUSED=$(free -m| grep  Mem | awk '{ print int($3/$2*100) }')
    echo "$MEMUSED"
}

GetWindowTitle() {
    WIN_TITLE=$(xdotool getactivewindow getwindowname)
    echo $WIN_TITLE
}

GetCpuUsage() {
    CPU_USAGE=$(bash ~/.config/lemonbar/scripts/cpu.sh)
    echo $CPU_USAGE
}

GetCpuAvgTemp() {
    CPU_TEMP=$(python ~/.config/lemonbar/scripts/get_avg_cpu_temp.py)
    echo $CPU_TEMP
}

while true; do 
    echo -e "%{l}%{F#D3F9FB}$(GetWindowTitle)%{F-}%{F#686868} ${SEP_ICON_LEFT} %{F-}%{r}%{F#686868} ${SEP_ICON_RIGHT}%{F-} cpu:$(GetCpuUsage)% %{F#686868}${SEP_ICON_RIGHT}%{F-} temp:$(GetCpuAvgTemp)C %{F#686868}${SEP_ICON_RIGHT}%{F-} memused:$(GetMemory)% %{F#686868}${SEP_ICON_RIGHT}%{F-} date:$(GetDate) %{F#686868}${SEP_ICON_RIGHT}%{F-} time:$(GetTime) %{F#686868}${SEP_ICON_RIGHT}%{F-} vol:$(GetVolume) %{F-}%{B-}"
    sleep 0.3;
done
