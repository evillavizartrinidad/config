#!/usr/bin/env bash
set -e
source ./config.sh

# First bar
lemonbar -g "${LB_L_W}x${LB_L_H}+${LB_L_X}+${LB_L_Y}"   \
    -o "MainLemonBar"                                      \
    -f "${LB_L_FONT}"                                      \ 
    -B "${LB_L_COLOR_BG}"                                  \
    -F "${LB_L_COLOR_FG}"                                  \
    -U "${LB_L_COLOR_U}" -p #-b
# Second bar
#./lemonbar ...
