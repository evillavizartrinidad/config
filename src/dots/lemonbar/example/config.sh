# Lemonbar Config

# Bar *base* positions (Per monitor)
# NOTE: Top left corner of each display is it's own
# x,y origin point on left and right displays
# NOTE: Top left corner of each display is it's own
# x,y origin point.
# LEFT BAR
LB_L_XB=0 # X
LB_L_YB=0 # Y
# RIGHT BAR
LB_R_XB=0 # X
LB_R_YB=0 # Y

# ---------------------------------------------
# Bar *base positions (Monitor inclusive)
# NOTE: Only one origin point set to 0,0 which
# is on the display with the first display id.
# Per monitor rules for previous config values
# do not apply. If one of these values are
# > 0, previous base positional values will
# be ignored and these will be used instead.
# Using these values over the previous ones
# could be useful if you wanted a single bar 
# that spanned multiple monitors, for example.
# LEFT BAR
LB_L_XIB=0 # X
LB_L_YIB=0 # Y
# RIGHT BAR
LB_R_XIB=0 # X
LB_R_YIB=0 # Y

if [[ $LB_L_XIB -ne 0 ]]; then
    LB_L_XB=$LB_L_XIB
if [[ $LB_L_YIB -ne 0 ]]; then
    LB_L_YB=$LB_L_YIB
if [[ $LB_R_XIB -ne 0 ]]; then
    LB_R_XB=$LB_R_XIB
if [[ $LB_R_YIB -ne 0 ]]; then
    LB_R_YB=$LB_R_YIB
fi

# ---------------------------------------------
# Dimensions of left and right displays
# LEFT DISPLAY
LB_L_DW=2560 # Width
LB_L_DH=1440 # Height
# RIGHT DISPLAY
LB_R_DW=1920 # Width
LB_R_DH=1080 # Height

# ---------------------------------------------
# Bar margins of left and right displays
# NOTE: Margins act like inverted padding 
# and do alter the geometry of the bar.
# LEFT BAR
LB_L_MARGIN_L=5 # Left margin
LB_L_MARGIN_T=5 # Top margin
LB_L_MARGIN_R=5 # Right margin
LB_L_MARGIN_B=5 # Bottom margin
# RIGHT BAR
LB_R_MARGIN_L=5 # Left margin
LB_R_MARGIN_T=5 # Top margin
LB_R_MARGIN_R=5 # Right margin
LB_R_MARGIN_B=5 # Bottom margin

# ---------------------------------------------
# Bar alignments
# -----------
# Left: -1
# Center: 0
# Right: 1
# Top: -1
# Bottom: 1
# -----------
# TODO: Automatically calculate position
# based on if bar uses a left, right, or 
# center alignment.
# LEFT BAR
LB_L_ALIGN_LR=-1 # Left/right
LB_L_ALIGN_TB=-1 # Top/bottom
# RIGHT BAR
LB_R_ALIGN_LR=-1 # left/right
LB_R_ALIGN_TB=-1 # Top/bottom

# ---------------------------------------------
# *Base* bar sizes
# LEFT BAR
LB_L_WB=$LB_L_DW
LB_L_HB=$LB_L_DH
# RIGHT BAR
LB_R_WB=$LB_R_DW
LB_R_HB=$LB_R_HW

# ---------------------------------------------
# *Calculated* sizes with margins acknowledged for left and right bars
# LEFT BAR
LB_L_W=$(( LB_L_DW - ( LB_L_MARGIN_L + LB_L_MARGIN_R ) )) # Width
LB_L_H=$(( LB_L_DH - ( LB_L_MARGIN_T + LB_L_MARGIN_B ) )) # Height
# RIGHT BAR
LB_R_W=$(( LB_R_DW - ( LB_R_MARGIN_L + LB_R_MARGIN_R ) )) # Width
LB_R_H=$(( LB_L_DH - ( LB_R_MARGIN_T + LB_R_MARGIN_B ) )) # Height

# ---------------------------------------------
# *Absolute* positions for left and right bars
# LEFT BAR
LB_L_X=$(( LB_L_XB + LB_L_MARGIN_L )) # X
LB_L_Y=$(( LB_L_YB + LB_L_MARGIN_T )) # Y
# RIGHT BAR
LB_R_X=$(( LB_R_XB + LB_R_MARGIN_L )) # X
LB_R_Y=$(( LB_R_YB + LB_R_MARGIN_T )) # Y

# ---------------------------------------------
# Colors for left and right bars
# LEFT BAR
LB_L_COLOR_BG="#202734" # Background
LB_L_COLOR_FG="#CBCCC6" # Foreground
LB_L_COLOR_U="#535C7D" # Underline
# RIGHT BAR
LB_R_COLOR_BG="#202734" # Background
LB_R_COLOR_FG="#CBCCC6" # Foreground
LB_R_COLOR_U="#535C7D" # Underline

# ---------------------------------------------
# Names for left and right bars' font
# LEFT BAR
LB_L_FONT="Hack\ Nerd\ Font\ Mono"
# RIGHT BAR
LB_R_FONT="Hack\ Nerd\ Font\ Mono"

