#!/usr/bin/bash
pkill lemonbar
bash config.sh | lemonbar -p -g "2560x22+0+0" \
    -o "HDMI-0" \
    -f "Hack Nerd Font Mono" \
    -f "FontAwesome" \
    -B "#191E2A" \
    -F "#D3F9FB" \
    -U "#172028" \
    -u 1 &
bash config.sh | lemonbar -p -g "1920x22+2560+0" \
    -o "DVI-D-0" \
    -f "Hack Nerd Font Mono" \
    -f "FontAwesome" \
    -B "#191E2A" \
    -F "#D3F9FB" \
    -U "#172028" \
    -u 1 &
