" [ SETTINGS ]
set noswapfile
set nobackup
set encoding=UTF-8
set showmatch
set hlsearch
set expandtab
set shiftwidth=4
set autoindent
set number
set ruler
set cursorline
set guifont=Monoid\ Nerd\ Font\ Mono\ 16
syntax enable
filetype plugin indent on
filetype plugin on

" [ PLUGINS ]
call plug#begin(stdpath('config') . '/plugged')

Plug 'junegunn/vim-easy-align'
Plug 'dense-analysis/ale'
Plug 'jiangmiao/auto-pairs'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'yggdroot/indentline'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-repeat'
Plug 'Pocco81/HighStr.nvim'
Plug 'folke/which-key.nvim'
Plug 'Yggdroot/indentLine'
Plug 'yonderbread/vim-pigments'

" [ COLOR SCHEME PLUGS ]
Plug 'sonph/onehalf', { 'rtp': 'vim' }
Plug 'dylanaraps/crayon'
Plug 'jacoborus/tender.vim'
Plug 'ayu-theme/ayu-vim'

" [ COC PLUG EXTENSIONS ]
Plug 'hallzy/gravity.vim'
Plug 'linkinpark342/xonsh-vim'
Plug 'daveyarwood/vim-alda'
Plug 'jceb/vim-orgmode'

call plug#end()

" [ COLORS ]
set termguicolors
let ayucolor="mirage"
colorscheme ayu

" [ NEOCLIDE/COC.NVIM ]
let g:coc_global_extensions = ['coc-json', 'coc-git', 'coc-cmake', 'coc-css', 
    \ 'coc-discord', 'coc-elixir', 'coc-eslint', 'coc-flutter', 'coc-gist', 'coc-go',
    \ 'coc-graphql', 'coc-html', 'coc-htmldjango', 'coc-java', 'coc-markdownlint',
    \ 'coc-powershell', 'coc-prettier',
    \ 'coc-sh', 'coc-svg', 'coc-sql', 'coc-toml', 'coc-xml', 'coc-yaml']

" [ AYU-THEME/AYU-VIM ]
let ayu_comment_italics=1
let ayu_string_italics=0
let ayu_type_italic=1
let ayu_keyword_italic=0

" [ YGGDROOT/INDENTLINE ]
let g:indentLine_enabled = 1
let g:indentLine_char_list = ['𝄀', '𝄁', '𝄂', '𝄃']
